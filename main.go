package main

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	vmhelpers "gitlab.com/k33g_org/side-projects/galago/galago-executor/api/executor"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// target is file path
func fetchWasmFile(url string, target string) (err error) {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(target)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err

}



// TODO: use an optional header token to authenticate the registry connection (before, implement it on the registry)

// ??? how to pass a set of environment variable to the runner, then to the function (TOKEN, etc...) ?
func main() {
	// WASM_EXECUTOR_HTTP=9090 WASM_FILE_URL="http://localhost:9999/hello/hello.wasm" ./galago-wasm-runner hello-function/hello.wasm
	// WASM_EXECUTOR_HTTP=9090 ./galago-wasm-runner hello-function/hello.wasm http://localhost:9999/hello/hello.wasm
	// WASM_EXECUTOR_HTTP=9090 ./galago-wasm-runner functions/hello.wasm http://localhost:9999/hello/hello.wasm

	wasmFilePath := os.Args[1]

	// check if the file exist
	if _, err := os.Stat(wasmFilePath); os.IsNotExist(err) {
		// if not, download it
		log.Println("❌ file does not exist")

		wasmDirectory := filepath.Dir(wasmFilePath)
		wasmFile := filepath.Base(wasmFilePath)

		wasmFileUrlArg := os.Args[2]

		wasmFileUrl := getEnv("WASM_FILE_URL", wasmFileUrlArg)
		if wasmFileUrl == "" {
			log.Println("❌ no wasm file url")
			os.Exit(1)
		}

		log.Println("🌍 downloading", wasmFile, "to", wasmDirectory)

		err := fetchWasmFile(wasmFileUrl, wasmFilePath)

		if err != nil {
			log.Println("❌ issue when downloading", wasmFile)
			os.Exit(1)
		} else {
			log.Println("🎉", wasmFile, "downloaded with success")
		}
	}

	// then load the wasmfile
	wasmVM, _ := vmhelpers.InitializeVMFromWasmFile(wasmFilePath)
	// TODO: handle errors

	service := func(response http.ResponseWriter, request *http.Request) {
		// TODO: content type -> JSON UTF8
		body, _ := ioutil.ReadAll(request.Body)
		//fmt.Println("body: " + string(body))

		defer request.Body.Close()

		result, _ := wasmVM.ExecuteFunction("handle", string(body))

		response.WriteHeader(http.StatusOK)
		response.Write([]byte(result))
	}

	healthCheck := func(response http.ResponseWriter, request *http.Request) {
		response.WriteHeader(http.StatusOK)
		response.Write([]byte(`{"healthCheck":"ok"}`))
	}

	http.HandleFunc("/", service)
	http.HandleFunc("/health", healthCheck)
	// TODO: metrics

	// gracefully terminate the process
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, os.Interrupt)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		select {
		case sig := <-gracefulStop:
			log.Println("✋ Aborting... Signal:", sig)
			time.Sleep(3 * time.Second)
			os.Exit(0) // ??? 0 or 1 ?
			//os.Exit(1)
		}
	}()

	httpPort := getEnv("WASM_EXECUTOR_HTTP", "8080")

	// ??? how to andle errors if the http port is already used ?
	log.Println("🌍 Listening on", httpPort)
	http.ListenAndServe(":"+httpPort, nil)

	// WASM_EXECUTOR_HTTP=9090 ./galago-wasm-runner hello-function/hello.wasm
	// curl -X POST -d '{"FirstName": "Bob", "LastName": "Morane"}' http://localhost:9090

}
