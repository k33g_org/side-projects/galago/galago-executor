# Galago Wasm runner

Developers must install the WasmEdge shared library with the same WasmEdge-go release version.

```bash
wget -qO- https://raw.githubusercontent.com/WasmEdge/WasmEdge/master/utils/install.sh | bash -s -- -v 0.9.0
source /home/gitpod/.wasmedge/env 
go mod init wasmedge-func
go get github.com/second-state/WasmEdge-go/wasmedge@v0.9.0
```

## Build and run the program

```bash
go build
WASM_EXECUTOR_HTTP=9090 ./galago-executor hello-function/hello.wasm
WASM_EXECUTOR_HTTP=9091 ./galago-executor yo-function/yo.wasm

# call the function
curl -X POST -d '{"FirstName": "Bob", "LastName": "Morane"}' http://localhost:9090
```

## Launch the runner with an url to the wasm file

### Requirements: you need to "http serve" the wasm file

```bash
# ------------------------------------
# Install Galago Registry 
# (with samples)
# ------------------------------------
git clone --depth=1 https://gitlab.com/k33g_org/side-projects/galago/galago-registry.git
cd galago-registry
go build
./galago-registry # default httport is 9999 or use this environment variable REGISTRY_HTTP
```

### Run it

```bash
WASM_EXECUTOR_HTTP=9090 ./galago-executor functions/hello.wasm https://localhost:9999/hello/hello.wasm
WASM_EXECUTOR_HTTP=9091 ./galago-executor functions/hey.wasm https://localhost:9999/hey/hey.wasm
```

**🖐 the file is downloaded only if it is not present in the `function` directory**

#### Call the functions

```bash
curl -X POST -d '{"FirstName": "John", "LastName": "Doe"}' http://localhost:9090
curl -X POST -d '{"FirstName": "Jane", "LastName": "Doe"}' http://localhost:9091
```


### Run Galago runner with a process name

```bash
processName="hello-function"
wasmFile="functions/hello.wasm"
wasmUrl="http://localhost:9999/hello/hello.wasm"
bash -c "WASM_EXECUTOR_HTTP=9090 exec -a ${processName} ./galago-executor ${wasmFile} ${wasmUrl}"
```

```bash
processName="hey-function"
wasmFile="functions/hey.wasm"
wasmUrl="http://localhost:9999/hey/hey.wasm"
bash -c "WASM_EXECUTOR_HTTP=9091 exec -a ${processName} ./galago-executor ${wasmFile} ${wasmUrl}"
```

#### Get the list of the galago-runner processes

```bash
ps -fC galago-executor

# output
UID          PID    PPID  C STIME TTY          TIME CMD
gitpod     48797   44678  0 07:20 pts/4    00:00:00 hello-function functions/hello.wasm http://localhost:9999/hello/hello.wasm
gitpod     48965   45151  0 07:20 pts/5    00:00:00 hey-function functions/hey.wasm http://localhost:9999/hey/hey.wasm
```

#### Kill a galago-runner process by name

```bash
pkill -f hello-function
```