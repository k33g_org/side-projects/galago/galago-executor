package helpers

import (
	"C"
	"strings"
	"unsafe"
)

func FromInt32PtrToString(value *int32) string {
	nth := 0
	var valueStr strings.Builder
	pointer := uintptr(unsafe.Pointer(value))
	for {
		s := *(*int32)(unsafe.Pointer(pointer + uintptr(nth)))
		if s == 0 {
			break;
		}

		valueStr.WriteByte(byte(s))
		nth++
	}

	output := valueStr.String()
	return output
}

func FromStringToBytePtr(returnValue string) *byte {
	return &(([]byte)(returnValue)[0])
}

func Use(funktion func(body string) string, parameters *int32) *byte {
	funktionResult := funktion(FromInt32PtrToString(parameters))
	return FromStringToBytePtr(funktionResult)
}

