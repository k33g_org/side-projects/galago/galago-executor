module gitlab.com/k33g_org/side-projects/galago/galago-executor

go 1.17

require (
	github.com/second-state/WasmEdge-go v0.9.0 // indirect
	gitlab.com/k33g_org/side-projects/galago/galago-executor/api/executor v0.0.0-20220310070626-33a1c25bfa92 // indirect
)
