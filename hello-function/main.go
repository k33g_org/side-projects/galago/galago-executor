package main

import (
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"

	//"gitlab.com/k33g_org/side-projects/galago/galago-executor/api/function"
	"gitlab.com/k33g_org/side-projects/galago/galago-executor/api/function"
)

func main() {}

func hello(body string) string {
	firstName := gjson.Get(body, "FirstName")
	lastName := gjson.Get(body, "LastName")

	result, _ := sjson.Set(`{"message":""}`, "message", "Hello " + firstName.Str + " " + lastName.Str)

	return result
}

//export handle
func handle(parameters *int32) *byte {
	return helpers.Use(hello, parameters)
}
