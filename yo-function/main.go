package main

import (
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/k33g_org/side-projects/galago/galago-executor/api/function"
)


func main() {}

func yo(body string) string {
	firstName := gjson.Get(body, "FirstName")
	lastName := gjson.Get(body, "LastName")

	result, _ := sjson.Set(`{"message":""}`, "message", "Yo " + firstName.Str + " " + lastName.Str)

	return result
}

//export handle
func handle(parameters *int32) *byte {
	return helpers.Use(yo, parameters)
}
