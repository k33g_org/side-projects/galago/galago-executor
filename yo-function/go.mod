module gitlab.com/k33g/yo-function

go 1.17

require (
	github.com/tidwall/gjson v1.14.0
	github.com/tidwall/sjson v1.2.4
	gitlab.com/k33g_org/side-projects/galago/galago-executor/api/function v0.0.0-20220316023505-8c1225188c65
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
